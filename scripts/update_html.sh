#!/bin/sh

export GIT_SSH_COMMAND='ssh -i /ssh-keys/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
mkdir -p /var/www/skiler.pl
cd /var/www/skiler.pl || exit 1
umask 0022
rsync -a /initial-html/ /var/www/skiler.pl
git reset --hard HEAD
git clean -fdx
git pull
chmod a-w /var/www/skiler.pl/sync/cgi-bin
