#!/bin/sh

/update_html.sh

if [ ! -z "$MR_UID" ]; then
    usermod -u $MR_UID mr > /dev/null 2>&1
    chown -R mr:mr /var/www/skiler.pl
fi

cd /var/www/skiler.pl/__CONFIG__
if [ $? -eq 0 ]; then
    rm /etc/apache2/sites-enabled/*
    cp * /etc/apache2/sites-enabled/
fi
exec /apache.sh
