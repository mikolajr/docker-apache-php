#!/bin/sh

cd /var/www/skiler.pl/__CONFIG__
[ $? -ne 0 ] && exit
if [ ! -z "`ls`" ]; then
	echo "Apache configuration found"
	rm /etc/apache2/sites-enabled/*
	cp * /etc/apache2/sites-enabled/
	echo "Reloading Apache"
	kill -HUP `cat /var/run/apache2/apache2.pid`
fi
