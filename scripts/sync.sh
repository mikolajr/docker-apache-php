#!/bin/sh

if [ `whoami` != 'mr' ]; then
    su mr -c $0 $*
    exit
fi

cd /var/www/skiler.pl
export GIT_SSH_COMMAND='ssh -i /ssh-keys/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
git reset --hard HEAD
git clean -fd
git pull
