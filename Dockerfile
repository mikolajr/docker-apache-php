FROM ubuntu:16.04

MAINTAINER Mikolaj Rydzewski <mikolaj.rydzewski@gmail.com>

RUN \
    apt-get update && \
    apt-get install -y \
	ca-certificates \
	git \
	vim \
	curl \
	xz-utils \
	wget \
	apache2 \
	apache2-suexec-custom \
	php \
	php-pear \
	php-pgsql \
	libapache2-mod-php7.0 \
	sudo \ 
	zip \
    && \
    apt-get clean && \
    sed -i 's/short_open_tag = Off/short_open_tag = On/' /etc/php/7.0/apache2/php.ini && \
    a2enmod cgi && \
    a2enmod suexec && \
    a2enmod rewrite && \
    a2enmod proxy && \
    a2enmod proxy_ajp && \
    useradd mr && \
    echo "mr ALL = (ALL) NOPASSWD: /script.sh" >> /etc/sudoers

ADD scripts/script.sh /
ADD scripts/start_apache.sh /
ADD scripts/sync.sh /
ADD scripts/apache.sh /
ADD scripts/update_html.sh /

EXPOSE 80

ENV MR_UID=1000

CMD /start_apache.sh
